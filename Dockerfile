FROM python:3.7-alpine3.9
LABEL maintainer="Igor Nehoroshev <mail@neigor.me>"

ENV DEVPISERVER_SERVERDIR="/data/server"
ENV DEVPI_CLIENTDIR="/data/client"

COPY entrypoint.sh /entrypoint.sh

RUN apk add --no-cache build-base libffi-dev \
    && pip3 install --no-cache-dir --no-use-pep517 devpi-server devpi-client \
    && apk del build-base \
    && mkdir /data

WORKDIR /data

CMD /entrypoint.sh
